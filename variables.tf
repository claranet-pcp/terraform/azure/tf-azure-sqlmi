//region: Networking
variable "location" {
    description = "The region for the MI and supporting resources to be created."
  type        = "string"
}

variable "resource_group_name" {
    description = "Name of the resource group to contain the MI resources."
  type        = "string"

}

variable "mi_subnet_id" {
      description = "The Azure ID of the the subnet to contain the MI instances."
  type        = "string"
}

variable "mi_subnet_prefix" {
        description = "The cidr of the subnet to contain the the MI instances."
  type        = "string"
}

variable "mi_prefix" {
          description = "The naming prefix to the MI resources"
  type        = "string"
}



variable "mi_routes" {
   description = "The UDR routes provided by Microsoft for MI traffic"
    type = "list"
    default = [
        "103.25.156.0/22",
          "103.255.140.0/22",
            "103.36.96.0/22",
              "103.9.8.0/22",
                "104.146.0.0/15",
                  "104.208.0.0/13",
                    "104.40.0.0/13",
                      "111.221.16.0/20",
                        "111.221.64.0/18",
                          "129.75.0.0/16",
                            "13.104.0.0/14",
                              "13.64.0.0/11",
                                "13.96.0.0/13",
                                  "131.253.0.0/16",
                                    "132.245.0.0/16",
                                      "134.170.0.0/16",
                                        "134.177.0.0/16",
                                          "137.116.0.0/15",
                                            "137.135.0.0/16",
                                            "138.196.0.0/16",
          "138.91.0.0/16",
            "139.217.0.0/16",
              "139.219.0.0/16",
                "141.251.0.0/16",
                  "146.147.0.0/16",
                    "147.243.0.0/16",
                      "150.171.0.0/16",
                        "150.242.48.0/22",
                          "157.54.0.0/15",
                            "157.56.0.0/14",
                              "157.60.0.0/16",
                                "167.220.0.0/16",
                                  "168.61.0.0/16",
                                    "168.62.0.0/15",
                                      "191.232.0.0/13",
                                        "192.100.102.0/24",
                                          "192.100.103.0/24",
                                            "192.197.157.0/24",
                                                      "192.32.0.0/16",
            "192.48.225.0/24",
              "192.84.159.0/24",
                "192.84.160.0/23",
                  "193.149.64.0/19",
                    "193.221.113.0/24",
                      "194.110.197.0/24",
                        "194.69.96.0/19",
                          "198.105.232.0/22",
                            "198.200.130.0/24",
                              "198.206.164.0/24",
                                "199.103.122.0/24",
                                  "199.103.90.0/23",
                                    "199.242.32.0/20",
                                      "199.242.48.0/21",
                                        "199.60.28.0/24",
                                          "199.74.210.0/24",
                                            "20.0.0.0/8",

              "202.89.224.0/20",
                "204.13.120.0/21",
                  "204.14.180.0/22",
                    "204.152.140.0/23",
                      "204.152.18.0/23",
                        "204.231.192.0/24",
                          "204.231.194.0/23",
                            "204.231.197.0/24",
                              "204.231.198.0/23",
                                "204.231.200.0/21",
                                  "204.231.208.0/20",
                                    "204.231.236.0/24",
                                      "204.79.135.0/24",
                                        "204.79.179.0/24",
                                          "204.79.181.0/24",
                                            "204.79.188.0/24",

                "204.79.195.0/24",
                  "204.79.196.0/23",
                    "204.79.252.0/24",
                      "205.174.224.0/20",
                        "206.138.168.0/21",
                          "206.191.224.0/19",
                            "207.46.0.0/16",
                              "207.68.128.0/18",
                                "208.68.136.0/21",
                                  "208.76.44.0/22",
                                    "208.84.0.0/21",
                                      "209.240.192.0/19",
                                        "213.199.128.0/18",
                                          "216.220.208.0/20",
                                            "216.32.180.0/22",
                  "23.96.0.0/13",
                    "40.64.0.0/10",
                      "42.159.0.0/16",
                        "51.0.0.0/8",
                          "52.0.0.0/8",
                            "64.4.0.0/18",
                              "65.52.0.0/14",
                                "66.119.144.0/20",
                                  "70.37.128.0/18",
                                    "70.37.0.0/17",
                                      "91.190.216.0/21",
                                        "94.245.64.0/18",

    ]
    
}

//region: Security

variable "nsg_enabled" {
  description = "Enable creation of the recommended MS NSG rules"
  type = "string"
  default = 1
}

//region: Managed Instance

            		variable "skuName" {
                   description = "SKU of the MI to build"
                  type = "string"
                  default = "GP_Gen5"
                }

                variable "skuEdition" {
                  description = "SKU  Edition of the MI to build"
                  type = "string"
                  default = "GeneralPurpose"
                }

                variable "administratorLogin" {
                  description = "SQL Administrator Username"
                  type = "string"
                  default = "sqladmin"
                }

                variable "administratorLoginPassword" {
                  description = "SQL Administrator Password"
                  type = "string"
                }



                                variable "storageSizeInGB" {
                                  description = "Size of the Storage in the MI cluster"
                  type = "string"
                }


variable "vCores" {
description = "Number of Vcores assigned to MI instances"
type = "string"
}


variable "licenseType" {
  description = "licence of SQL used"
type = "string"
default = "LicenseIncluded"
}

variable "hardwareFamily" {
    description = "Generation of MI - Gen4 or Gen5"
type = "string"
default = "Gen5"
}

variable "dnsZonePartner" {
type = "string"
default = ""
}

variable "collation" {
      description = "SQL Collation used"
type = "string"
default = "SQL_Latin1_General_CP1_CI_AS"
}

variable "proxyOverride" {
      description = "Proxy or redirect access to MI instances"
type = "string"
default = "Redirect"
}


variable "publicDataEndpointEnabled" {
      description = "Use public endpoint for MI"
type = "string"
default = "False"
}

variable "timezoneId" {
      description = "Time Zone Used"
type = "string"
default = "GMT Standard Time"
}



