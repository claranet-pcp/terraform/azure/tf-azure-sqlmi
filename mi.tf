data "template_file" "arm_template" {
  template = "${file("${path.module}/includes/arm.tmpl")}"
  
  vars {
                  	managedInstanceName = "${var.mi_prefix}-mi"
            		location = "${var.location}",
            		skuName = "${var.skuName}",
            		skuEdition = "${var.skuEdition}",
            		administratorLogin = "${var.administratorLogin}",
            		administratorLoginPassword = "${var.administratorLoginPassword}",
            		subnetId = "${var.mi_subnet_id}",
            		storageSizeInGB = "${var.storageSizeInGB}",
            		vCores = "${var.vCores}",
            		licenseType = "${var.licenseType}",
            		hardwareFamily = "${var.hardwareFamily}",
            		dnsZonePartner = "${var.dnsZonePartner}",
            		collation = "${var.collation}",
            		proxyOverride = "${var.proxyOverride}",
            		publicDataEndpointEnabled = "${var.publicDataEndpointEnabled}",
            		timezoneId = "${var.timezoneId}"
  }
}

resource "azurerm_template_deployment" "arm" {
  name                = "ManagedInstance"
  resource_group_name = "${var.resource_group_name}"
  template_body       = "${data.template_file.arm_template.rendered}"
  deployment_mode     = "Incremental"
}
