### Default MS Rules
### https://docs.microsoft.com/en-us/azure/sql-database/sql-database-managed-instance-connectivity-architecture#high-level-connectivity-architecture
### DO NOT CHANGE

resource "azurerm_network_security_group" "mi_nsg" {
  name                = "${var.mi_prefix}-mi-nsg"
  count = "${var.nsg_enabled}"
  location                      = "${var.location}"
   resource_group_name = "${var.resource_group_name}"
}


#Inbound 

resource "azurerm_network_security_rule" "allow_management_inbound" {
    count = "${var.nsg_enabled}"
  name                        = "allow_management_inbound"
  priority                    = "100"
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "tcp"
  source_port_range           = "*"
  destination_port_ranges      = ["9000","9003","1438","1440","1452"]
  source_address_prefix       = "*"
  description = "Allow inbound management traffic"
  destination_address_prefix  = "${var.mi_subnet_prefix}"
  resource_group_name         = "${var.resource_group_name}"
  network_security_group_name = "${azurerm_network_security_group.mi_nsg.name}"
}

resource "azurerm_network_security_rule" "allow_misubnet_inbound" {
    count = "${var.nsg_enabled}"
  name                        = "allow_misubnet_inbound"
  priority                    = "200"
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    =  "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "${var.mi_subnet_prefix}"
  description = "Allow inbound traffic inside the subnet"
  destination_address_prefix  = "${var.mi_subnet_prefix}"
  resource_group_name         = "${var.resource_group_name}"
  network_security_group_name = "${azurerm_network_security_group.mi_nsg.name}"
}

resource "azurerm_network_security_rule" "allow_health_probe_inbound" {
    count = "${var.nsg_enabled}"
  name                        = "allow_health_probe_inbound"
  priority                    = "300"
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    =  "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "AzureLoadBalancer"
  description = "Allow health probe"
  destination_address_prefix  = "${var.mi_subnet_prefix}"
  resource_group_name         = "${var.resource_group_name}"
  network_security_group_name = "${azurerm_network_security_group.mi_nsg.name}"
}

resource "azurerm_network_security_rule" "allow_tds_inbound" {
    count = "${var.nsg_enabled}"
  name                        = "allow_tds_inbound"
  priority                    = "1000"
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    =  "TCP"
  source_port_range           = "*"
  destination_port_range      = "1433"
  source_address_prefix       = "VirtualNetwork"
  description = "Allow access to data"
  destination_address_prefix  = "${var.mi_subnet_prefix}"
  resource_group_name         = "${var.resource_group_name}"
  network_security_group_name = "${azurerm_network_security_group.mi_nsg.name}"
}

resource "azurerm_network_security_rule" "allow_redirect_inbound" {
    count = "${var.nsg_enabled}"
  name                        = "allow_redirect_inbound"
  priority                    = "1100"
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    =  "TCP"
  source_port_range           = "*"
  destination_port_range      = "11000-11999"
  source_address_prefix       = "VirtualNetwork"
  description = "Allow inbound redirect traffic to Managed Instance inside the virtual network"
  destination_address_prefix  = "${var.mi_subnet_prefix}"
  resource_group_name         = "${var.resource_group_name}"
  network_security_group_name = "${azurerm_network_security_group.mi_nsg.name}"
}
resource "azurerm_network_security_rule" "allow_geodr_inbound" {
    count = "${var.nsg_enabled}"
  name                        = "allow_geodr_inbound"
  priority                    = "1200"
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    =  "TCP"
  source_port_range           = "*"
  destination_port_range      = "5022"
  source_address_prefix       = "VirtualNetwork"
  description = "Allow inbound geodr traffic inside the virtual network"
  destination_address_prefix  = "${var.mi_subnet_prefix}"
  resource_group_name         = "${var.resource_group_name}"
  network_security_group_name = "${azurerm_network_security_group.mi_nsg.name}"
}


resource "azurerm_network_security_rule" "deny_all_inbound" {
    count = "${var.nsg_enabled}"
  name                        = "deny_all_inbound"
  priority                    = "4096"
  direction                   = "Inbound"
  access                      = "Deny"
  protocol                    =  "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  description = "Deny all other inbound traffic"
  destination_address_prefix  = "*"
  resource_group_name         = "${var.resource_group_name}"
  network_security_group_name = "${azurerm_network_security_group.mi_nsg.name}"
}





#Outbound

resource "azurerm_network_security_rule" "allow_management_outbound" {
    count = "${var.nsg_enabled}"
  name                        = "allow_management_outbound"
  priority                    = "100"
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    =  "TCP"
  source_port_range           = "*"
  destination_port_ranges     = ["443","12000"]
  source_address_prefix       = "${var.mi_subnet_prefix}"
  description = "Allow outbound management traffic"
  destination_address_prefix  = "AzureCloud"
  resource_group_name         = "${var.resource_group_name}"
  network_security_group_name = "${azurerm_network_security_group.mi_nsg.name}"
}

resource "azurerm_network_security_rule" "allow_misubnet_outbound" {
    count = "${var.nsg_enabled}"
  name                        = "allow_misubnet_outbound"
  priority                    = "200"
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    =  "*"
  source_port_range           = "*"
  destination_port_range     =  "*"
  source_address_prefix       = "${var.mi_subnet_prefix}"
  description = "Allow outbound traffic inside the subnet"
  destination_address_prefix  = "${var.mi_subnet_prefix}"
  resource_group_name         = "${var.resource_group_name}"
  network_security_group_name = "${azurerm_network_security_group.mi_nsg.name}"
}

resource "azurerm_network_security_rule" "allow_linkedserver_outbound" {
    count = "${var.nsg_enabled}"
  name                        = "allow_linkedserver_outbound"
  priority                    = "1000"
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    =  "TCP"
  source_port_range           = "*"
  destination_port_range     =  "1433"
  source_address_prefix       = "${var.mi_subnet_prefix}"
  description = "Allow outbound linkedserver traffic inside the virtual network"
  destination_address_prefix  = "VirtualNetwork"
  resource_group_name         = "${var.resource_group_name}"
  network_security_group_name = "${azurerm_network_security_group.mi_nsg.name}"
}

resource "azurerm_network_security_rule" "allow_redirect_outbound" {
    count = "${var.nsg_enabled}"
  name                        = "allow_redirect_outbound"
  priority                    = "1100"
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    =  "TCP"
  source_port_range           = "*"
  destination_port_range     =  "11000-11999"
  source_address_prefix       = "${var.mi_subnet_prefix}"
  description = "Allow outbound redirect traffic to Managed Instance inside the virtual network"
  destination_address_prefix  = "VirtualNetwork"
  resource_group_name         = "${var.resource_group_name}"
  network_security_group_name = "${azurerm_network_security_group.mi_nsg.name}"
}

resource "azurerm_network_security_rule" "allow_geodr_outbound" {
    count = "${var.nsg_enabled}"
  name                        = "allow_geodr_outbound"
  priority                    = "1200"
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    =  "TCP"
  source_port_range           = "*"
  destination_port_range     =  "5022"
  source_address_prefix       = "${var.mi_subnet_prefix}"
  description = "Allow outbound geodr traffic inside the virtual network"
  destination_address_prefix  = "VirtualNetwork"
  resource_group_name         = "${var.resource_group_name}"
  network_security_group_name = "${azurerm_network_security_group.mi_nsg.name}"
}

resource "azurerm_network_security_rule" "deny_all_outbound" {
    count = "${var.nsg_enabled}"
  name                        = "deny_all_outbound"
  priority                    = "4096"
  direction                   = "Outbound"
  access                      = "Deny"
  protocol                    =  "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  description = "Deny all other outbound traffic"
  destination_address_prefix  = "*"
  resource_group_name         = "${var.resource_group_name}"
  network_security_group_name = "${azurerm_network_security_group.mi_nsg.name}"
}
