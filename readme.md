

## Tf-Azure-Sql-Mi Parameters

  

| Name | Type | Description | Default | Mandatory |

|---|---|---|---|---|

| location | string | The region for the MI and supporting resources to be created. | | True |

| resource_group_name | string | Name of the resource group to contain the MI resources. | | True |

| mi_subnet_id | string | The Azure ID of the the subnet to contain the MI instances. | | True |

| mi_subnet_prefix | string | The cidr of the subnet to contain the the MI instances. | | True |

| mi_prefix | string | The naming prefix to the MI resources | | True |

| mi_routes | list | The UDR routes provided by Microsoft for MI traffic | | True |

| nsg_enabled | string | Enable creation of the recommended MS NSG rules | | True |

| skuName | string | SKU of the MI to build | GP_Gen5 | False |

| skuEdition | string | SKU Edition of the MI to build | GeneralPurpose | False |

| administratorLogin | string | SQL Administrator Username | sqladmin | False |

| administratorLoginPassword | string | SQL Administrator Password | | True |

| storageSizeInGB | string | Size of the Storage in the MI cluster | | True |

| vCores | string | Number of Vcores assigned to MI instances | | True |

| licenseType | string | licence of SQL used | LicenseIncluded | False |

| hardwareFamily | string | Generation of MI - Gen4 or Gen5 | Gen5 | False |

| dnsZonePartner | string | | | False |

| collation | string | SQL Collation used | SQL_Latin1_General_CP1_CI_AS | False |

| proxyOverride | string | Proxy or redirect access to MI instances | Redirect | False |

| publicDataEndpointEnabled | string | Use public endpoint for MI | False | False |

| timezoneId | string | Time Zone Used | GMT Standard Time | False |

  
  

Example:

  

    module "sql_mi" {
    
    source = "../../Modules/tf-azure-sql-mi"
    
    resource_group_name = "clara-we-prod-rg"
    
    location = "WestEurope"
    
    mi_subnet_id = ""/subscriptions/74aade1e-72a2-44fc-b4d4-995121ec7656/resourceGroups/claranet-uat-network-we-rg/providers/Microsoft.Network/virtualNetworks/claranet-prod-network-wevnet/subnets/websubnet
    
    mi_subnet_prefix = "10.0.1.0/28"
    
    mi_prefix = "clara-web-prod"
    
    administratorLogin = "supersecretlogin"
    
    administratorLoginPassword = "PassW0rd&"
    
    storageSizeInGB = "128"
    
    vCores = "8"
    
    }


