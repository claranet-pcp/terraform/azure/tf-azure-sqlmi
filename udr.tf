#Route Table
### Default MS Routes
### https://docs.microsoft.com/en-us/azure/sql-database/sql-database-managed-instance-connectivity-architecture#high-level-connectivity-architecture
### DO NOT CHANGE
resource "azurerm_route_table" "mi" {
  name                          = "${var.mi_prefix}-rt"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  disable_bgp_route_propagation = false

  }

  resource "azurerm_route" "subnet_to_vnetlocal" {
  name                = "subnet_to_vnetlocal"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      = "${var.mi_subnet_prefix}"
  next_hop_type       = "vnetlocal"
}

  resource "azurerm_route" "mi-nexthop-internet" {
  count = "${length(var.mi_routes)}"
  name                = "mi-nexthop-internet-${count.index}"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      = "${element(var.mi_routes, count.index)}"
  next_hop_type       = "internet"
}



##### SQL Management

resource "azurerm_route" "SqlManagement_automation_backup" {
  name                = "SqlManagement_automation_backup"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "51.142.215.251/32"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_automation_global" {
  name                = "SqlManagement_automation_global"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "104.214.108.80/32"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_automation_primary" {
  name                = "SqlManagement_automation_primary"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "51.140.127.51/32"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_controlPlane_az01" {
  name                = "SqlManagement_controlPlane_az01"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "51.140.146.224/27"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_controlPlane_az02" {
  name                = "SqlManagement_controlPlane_az02"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "51.105.66.192/27"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_controlPlane_az03" {
  name                = "SqlManagement_controlPlane_az03"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "51.105.74.192/27"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_controlPlane_backup1" {
  name                = "SqlManagement_controlPlane_backup1"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "51.105.64.0/32"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_controlPlane_primary" {
  name                = "SqlManagement_controlPlane_primary"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "51.140.184.11/32"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_deployment" {
  name                = "SqlManagement_deployment"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "65.55.188.0/24"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_security_backup" {
  name                = "SqlManagement_security_backup"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "51.141.11.149/32"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_security_global_1" {
  name                = "SqlManagement_security_global_1"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "52.179.184.76/32"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_security_global_2" {
  name                = "SqlManagement_security_global_2"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "52.187.116.202/32"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_security_global_3" {
  name                = "SqlManagement_security_global_3"
    route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "52.177.202.6/32"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_security_primary" {
  name                = "SqlManagement_security_primary"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "51.140.187.124/32"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_supportability_1" {
  name                = "SqlManagement_supportability_1"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "207.68.190.32/27"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_supportability_2" {
  name                = "SqlManagement_supportability_2"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "13.106.78.32/27"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_supportability_3" {
  name                = "SqlManagement_supportability_3"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "13.106.174.32/27"
  next_hop_type       = "internet"
}

resource "azurerm_route" "SqlManagement_supportability_4" {
  name                = "SqlManagement_supportability_4"
  route_table_name    = "${azurerm_route_table.mi.name}"
  resource_group_name           = "${var.resource_group_name}"
  address_prefix      =  "13.106.4.96/27"
  next_hop_type       = "internet"
}